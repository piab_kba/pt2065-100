EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:RJ12 J2
U 1 1 611A6C16
P 7100 3500
F 0 "J2" H 7157 4067 50  0000 C CNN
F 1 "RJ12" H 7157 3976 50  0000 C CNN
F 2 "Connector_RJ:RJ12_Amphenol_54601" V 7100 3525 50  0001 C CNN
F 3 "~" V 7100 3525 50  0001 C CNN
	1    7100 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 611A739C
P 4250 3450
F 0 "J1" H 4300 3867 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 4300 3776 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 4250 3450 50  0001 C CNN
F 3 "~" H 4250 3450 50  0001 C CNN
	1    4250 3450
	1    0    0    -1  
$EndComp
Text GLabel 4050 3250 0    50   Input ~ 0
MOSI
Text GLabel 4050 3450 0    50   Input ~ 0
RST
Text GLabel 4050 3550 0    50   Input ~ 0
SCK
Text GLabel 4050 3650 0    50   Input ~ 0
MISO
$Comp
L power:GND #PWR01
U 1 1 611A7FB5
P 4750 3750
F 0 "#PWR01" H 4750 3500 50  0001 C CNN
F 1 "GND" H 4755 3577 50  0000 C CNN
F 2 "" H 4750 3750 50  0001 C CNN
F 3 "" H 4750 3750 50  0001 C CNN
	1    4750 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3450 4750 3450
Wire Wire Line
	4750 3450 4750 3550
Wire Wire Line
	4550 3550 4750 3550
Connection ~ 4750 3550
Wire Wire Line
	4750 3550 4750 3650
Wire Wire Line
	4550 3650 4750 3650
Connection ~ 4750 3650
Wire Wire Line
	4750 3650 4750 3750
$Comp
L power:+5V #PWR02
U 1 1 611A8D30
P 4950 3250
F 0 "#PWR02" H 4950 3100 50  0001 C CNN
F 1 "+5V" H 4965 3423 50  0000 C CNN
F 2 "" H 4950 3250 50  0001 C CNN
F 3 "" H 4950 3250 50  0001 C CNN
	1    4950 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3250 4550 3250
Wire Wire Line
	4550 3350 4750 3350
Wire Wire Line
	4750 3350 4750 3450
Connection ~ 4750 3450
NoConn ~ 4050 3350
Text GLabel 7500 3500 2    50   Input ~ 0
MOSI
Text GLabel 7500 3400 2    50   Input ~ 0
SCK
Text GLabel 7500 3600 2    50   Input ~ 0
RST
Text GLabel 7500 3200 2    50   Input ~ 0
MISO
$Comp
L power:+5V #PWR03
U 1 1 611A99E5
P 7850 3300
F 0 "#PWR03" H 7850 3150 50  0001 C CNN
F 1 "+5V" H 7865 3473 50  0000 C CNN
F 2 "" H 7850 3300 50  0001 C CNN
F 3 "" H 7850 3300 50  0001 C CNN
	1    7850 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 611A9FB2
P 7850 3700
F 0 "#PWR04" H 7850 3450 50  0001 C CNN
F 1 "GND" H 7855 3527 50  0000 C CNN
F 2 "" H 7850 3700 50  0001 C CNN
F 3 "" H 7850 3700 50  0001 C CNN
	1    7850 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 3300 7850 3300
Wire Wire Line
	7850 3700 7500 3700
$EndSCHEMATC
